<% include Header %>
<main class="main">
    <div class="page-content">
        <section class="error-section d-flex flex-column justify-content-center align-items-center text-center pl-3 pr-3">
            <h1 class="mb-2 ls-m">Error 404</h1>
            <img src="$SiteSettings.Logo.ScaleMaxWidth(609).AbsoluteLink" alt="error 404">
            <h4 class="mt-7 mb-0 ls-m text-uppercase">Oops! That page can’t be found.</h4>
            <p class="text-grey font-primary ls-m">It looks like nothing was found at this location.</p>
            <a href="/" class="btn btn-primary mb-4">Go home</a>
        </section>
    </div>
</main>