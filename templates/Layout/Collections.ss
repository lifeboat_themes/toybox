<% include Header %>
<main class="main">
    <div class="page-content">
        <div class="container">
            <section class="mt-md-10 pt-md-3 mt-6 mb-md-10 mb-6 masonry-section">
                <div class="row grid gutter-sm">
                    <% loop $AllCollections %>
                    <div class="grid-item col-md-6 <% if $Odd %>height-x2<% else %>height-x1<% end_if %>>">
                        <div class="category category-banner category-absolute overlay-light">
                            <a href="$AbsoluteLink">
                                <figure>
                                    <% if $Odd %>
                                        <img src="$Image.Fill(585,397).AbsoluteLink" alt="$Collection.Title">
                                    <% else %>
                                        <img src="$Image.Fill(585,205).AbsoluteLink" alt="$Collection.Title">
                                    <% end_if %>
                                </figure>
                            </a>
                            <div class="category-content">
                                <h4 class="category-name">$Title</h4>
                                <span class="category-count"><span>$Products.count</span> Products </span>
                                <a href="$AbsoluteLink" class="btn-underline btn-link">Shop Now</a>
                            </div>
                        </div>
                    </div>
                    <% end_loop %>
                    <div class="col-1 grid-space"></div>
                </div>
            </section>
        </div>
    </div>
</main>