#Toybox

##Home Page

###Home Banner Collection
Up to 2 collections can be shown in a banner at the top of the home page in a carousel. These can be set in the custom fields labelled
**Home: Banner Collection 1, Home: Banner Collection 2**.

###Home Sidebar Collection
Up to 2 collections can be shown in the sidebar on the right of the homepage banner. These collections can be selected from the custom fields
labelled: **Home: Sidebar Collection 1, Home: Sidebar Collection 2**.

###Home Carousel Collection
Up to 4 collections can be chosen to be shown in a Carousel Collection each, in the homepage underneath the banner and sidebar collections.
These collections can be set in the custom fields labelled: **Home: Carousel Collection 1, Home: Carousel Collection 2, Home: Carousel Collection 3, Home: Carousel Collection 4**.

###Home Collection Card
Two collections can be shown in collection cards next to each other under the carousel collections. These can be set in the custom fields labelled
**Home: Collection Card Left, Home: Collection Card Right**.

###Home Display Collection
Up to 3 collections may be shown in the home page under the Collection cards in a Display Collection section. This will feature the Collection's image on the left with a carousel of products
from that collection on the right. These can be selected from the custom fields **Home: Display Collection 1, Home: Display Collection 2, Home: Display Collection 3**.

###Home Newsletter Section
If the store is integrated with mailchimp to send emails, a newsletter image can be set as a background image of the section.
This image can be set in the custom fields labelled **Newsletter Section Image**.

##Footer
### Footer: First Menu
You can select a menu to be displayed in the footer middle section. This will be the menu shown on the left of the three menu options.

_Note: Submenus items will not be displayed_

### Footer: Second Menu
You can select a menu to be displayed in the footer middle section. This will be the menu shown in the middle of the three menu options.

_Note: Submenus items will not be displayed_

### Footer: Third Menu
You can select a menu to be displayed in the footer middle section. This will be the menu shown on the right of the three menu options.

_Note: Submenus items will not be displayed_

##Contact Page
###Map Location
In the Contact page, you can have a map of the store displayed underneath the Contact form.
This can be done by going to the custom field labelled **Map Location** in the Design section and inputting the store name and full address in the field, in the format shown below.

**Store Name, Street Name, Location Post Code**

_NOTE: It is important to get these details exactly as they are shown on Google Maps._
